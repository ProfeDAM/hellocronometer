package com.test.profe.hellochronometer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView chrono = (TextView) findViewById (R.id.crono);
        Chronometer chronometer = new Chronometer(30000, 1000, chrono);
        chronometer.start();

    }
}
