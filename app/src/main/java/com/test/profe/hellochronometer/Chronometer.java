package com.test.profe.hellochronometer;

import android.os.CountDownTimer;
import android.widget.TextView;

public class Chronometer extends CountDownTimer {

    private TextView textViewChrono;


    public Chronometer(long millisInFuture, long countDownInterval, TextView textView) {
        super(millisInFuture, countDownInterval);
        textViewChrono = textView;
    }

    @Override
    public void onTick(long l) {

        textViewChrono.setText ("seconds remaining: " + l / 1000);

    }

    @Override
    public void onFinish() {
        textViewChrono.setText ("Done!");

    }
}
